<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>University of Maryland - Welcome to Terrapin Express</title>
    <link rel="stylesheet" href="assets/css/foundation.css" />
    <link rel="stylesheet" href="assets/css/style.css" />
    <script src="assets/js/vendor/modernizr.js"></script>
  </head>
  <body>
  <div class="off-canvas-wrap" data-offcanvas>
	<div class="inner-wrap">
        <nav class="tab-bar hide-for-medium-up">
            <section class="left-small">
                <a href="#" class="left-off-canvas-toggle menu-icon"><span></span></a>
            </section>
            <section class="middle tab-bar-section">
                <a href="#" class="left-off-canvas-toggle"><h1 class="title">MENU</h1></a>
            </section>
        </nav>
        <aside class="left-off-canvas-menu">
        	<ul class="off-canvas-list">
            	<li><a href="#">Test</a></li>
            </ul>
        </aside>
  	<section id="umd-header">
        <div class="row">
            <div class="small-12 columns">
                <a href="http://www.umd.edu/" id="umd-logo"><img src="assets/img/umd-logo.png" alt="University of Maryland" /></a>
            </div>
        </div>
    </section>
    <section id="page-header">
    	<div class="row">
        	<div id="container-header" class="small-12 columns text-center">
            	<img src="assets/img/header.gif" />
            </div>
        </div>
    </section>