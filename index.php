<?php include_once "header.php" ?>

        <div class="row">
            <div class="small-12 columns" id="container-body">
                <div class="row">
                  <div class="medium-3 columns show-for-medium-up">
                    <ul class="side-nav">
                      <li><a href="#">Home</a></li>
                      <li><a href="#">Check My Balance</a></li>
                      <li><a href="#">Open a New Account</a></li>
                      <li><a href="#">Add Money to My Account</a></li>
                      <li><a href="#">Report Lost Card</a></li>
                      <li><a href="#">Use Online Services</a></li>
                      <li><a href="#">Links</a></li>
                    </ul>
                  </div>
                  <div class="medium-9 columns">
                    <div id="content-body">
                    </div>
                  </div>
                </div>
            </div>
        </div>
<?php include_once "footer.php" ?>