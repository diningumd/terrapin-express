	<footer>
    	<div class="row">
        	<div id="container-footer" class="small-12 columns">
            	<div class="footer-module">
                    <p>&copy; Copyright 2014 University of Maryland, College Park. Terrapin Express is a Service Offered by Dining Services.</p>
                    <p>301-314-8068 | <a href="mailto:terpexp@dining.umd.edu">terpexp@dining.umd.edu</a></p>
                </div>
            </div>
        </div>
    </footer>
    
            <a class="exit-off-canvas"></a>
    </div>
</div>


	<script src="assets/js/vendor/jquery.js"></script>
    <script src="assets/js/foundation.min.js"></script>
    <script>
      $(document).foundation();
    </script>
  </body>
</html>

